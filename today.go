package datetime

import "time"

func Now() DateTime {
	var now DateTime
	now.Set(time.Now())
	return now
}

func Today() PureDate {
	var today PureDate
	today.Set(time.Now())
	return today
}
