package datetime

import (
	"fmt"
	"strings"
	"time"
)

const DATETIME_FORMAT string = "2006-01-02 15:04:05"

type DateTime time.Time

func (dt *DateTime) UnmarshalJSON(bs []byte) error {
	s := strings.Trim(string(bs), `"`)
	return dt.Assign(s)
}

func (dt DateTime) MarshalJSON() ([]byte, error) {
	s := fmt.Sprintf(`"%s"`, dt.String())
	return []byte(s), nil
}

func (dt DateTime) Format(fmt string) string {
	t := time.Time(dt)
	return t.Format(fmt)
}

func (dt DateTime) Time() time.Time {
	return time.Time(dt)
}

func (dt DateTime) String() string {
	return dt.Format(DATETIME_FORMAT)
}

func (dt *DateTime) Set(t time.Time) {
	*dt = DateTime(t)
}

func (dt *DateTime) Assign(s string) error {
	return dt.Parse(DATETIME_FORMAT, s)
}

func (dt *DateTime) Parse(fmt string, s string) error {
	t, e := time.ParseInLocation(fmt, s, time.Local)
	if e != nil {
		return e
	}
	dt.Set(t)
	return nil
}

func (dt DateTime) PureDate() PureDate {
	var pd PureDate
	pd.Set(dt.Time())
	return pd
}

func (dt DateTime) PureTime() PureTime {
	var pt PureTime
	pt.Set(dt.Time())
	return pt
}
