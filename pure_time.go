package datetime

import (
	"fmt"
	"strings"
	"time"
)

const PURE_TIME_FORMAT string = "15:04:05"

type PureTime time.Time

func (pt *PureTime) UnmarshalJSON(bs []byte) error {
	s := strings.Trim(string(bs), `"`)
	return pt.Assign(s)
}

func (pt PureTime) MarshalJSON() ([]byte, error) {
	s := fmt.Sprintf(`"%s"`, pt.String())
	return []byte(s), nil
}

func (pt PureTime) Format(fmt string) string {
	t := time.Time(pt)
	return t.Format(fmt)
}

func (pt PureTime) Time() time.Time {
	return time.Time(pt)
}

func (pt PureTime) String() string {
	return pt.Format(PURE_TIME_FORMAT)
}

func (pt *PureTime) Set(t time.Time) {
	tt := time.Date(0, 0, 0, t.Hour(), t.Minute(), t.Second(), t.Nanosecond(), t.Location())
	*pt = PureTime(tt)
}

func (pt *PureTime) Assign(s string) error {
	return pt.Parse(PURE_TIME_FORMAT, s)
}

func (pt *PureTime) Parse(fmt string, s string) error {
	t, e := time.ParseInLocation(fmt, s, time.Local)
	if e != nil {
		return e
	}
	pt.Set(t)
	return nil
}
