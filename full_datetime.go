package datetime

import (
	"fmt"
	"strings"
	"time"
)

const FULL_DATETIME_FORMAT string = "2006-01-02 15:04:05.999999999"

type FullDateTime time.Time

func (fdt *FullDateTime) UnmarshalJSON(bs []byte) error {
	s := strings.Trim(string(bs), `"`)
	return fdt.Assign(s)
}

func (fdt FullDateTime) MarshalJSON() ([]byte, error) {
	s := fmt.Sprintf(`"%s"`, fdt.String())
	return []byte(s), nil
}

func (fdt FullDateTime) Format(fmt string) string {
	t := time.Time(fdt)
	return t.Format(fmt)
}

func (fdt FullDateTime) Time() time.Time {
	return time.Time(fdt)
}

func (fdt FullDateTime) String() string {
	return fdt.Format(FULL_DATETIME_FORMAT)
}

func (fdt *FullDateTime) Set(t time.Time) {
	*fdt = FullDateTime(t)
}

func (fdt *FullDateTime) Assign(s string) error {
	return fdt.Parse(FULL_DATETIME_FORMAT, s)
}

func (fdt *FullDateTime) Parse(fmt string, s string) error {
	t, e := time.ParseInLocation(fmt, s, time.Local)
	if e != nil {
		return e
	}
	fdt.Set(t)
	return nil
}

func (fdt FullDateTime) Millisecond() int {
	return fdt.Time().Nanosecond() / int(time.Millisecond)
}

func (fdt FullDateTime) Microsecond() int {
	return fdt.Time().Nanosecond() / int(time.Microsecond)
}

func (fdt *FullDateTime) AddMillisecond(millisecond int) {
	fdt.Set(fdt.Time().Add(time.Millisecond * time.Duration(millisecond)))
}

func (fdt *FullDateTime) AddMicrosecond(microsecond int) {
	fdt.Set(fdt.Time().Add(time.Microsecond * time.Duration(microsecond)))
}

func (fdt FullDateTime) PureDate() PureDate {
	var pd PureDate
	pd.Set(fdt.Time())
	return pd
}

func (fdt FullDateTime) PureTime() PureTime {
	var pt PureTime
	pt.Set(fdt.Time())
	return pt
}
