package datetime

import (
	"fmt"
	"strings"
	"time"
)

const PURE_DATE_FORMAT string = "2006-01-02"

type PureDate time.Time

func (pd *PureDate) UnmarshalJSON(bs []byte) error {
	s := strings.Trim(string(bs), `"`)
	return pd.Assign(s)
}

func (pd PureDate) MarshalJSON() ([]byte, error) {
	s := fmt.Sprintf(`"%s"`, pd.String())
	return []byte(s), nil
}

func (pd PureDate) Format(fmt string) string {
	t := time.Time(pd)
	return t.Format(fmt)
}

func (pd PureDate) Time() time.Time {
	return time.Time(pd)
}

func (pd PureDate) String() string {
	return pd.Format(PURE_DATE_FORMAT)
}

func (pd *PureDate) Set(t time.Time) {
	td := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
	*pd = PureDate(td)
}

func (pd *PureDate) Assign(s string) error {
	return pd.Parse(PURE_DATE_FORMAT, s)
}

func (pd *PureDate) Parse(fmt string, s string) error {
	t, e := time.ParseInLocation(fmt, s, time.Local)
	if e != nil {
		return e
	}
	pd.Set(t)
	return nil
}
